

# burger-venue-web

This is frontend application for burger venue search.

# Project structure
```
apps/              contains main app that is run starting the application
libs/api           main enums and domain enities of the application used for communication with backend
libs/http-common   mapper for http requests
libs/venues        main components used for the application including venue loading from backend and displaying it
```

## Installation and usage

* Download the repo
* Install dependencies using `npm install`
* Run the application using `nx serve` or `npm run start`
* Go to http://localhost:4200/

## Running application using docker

* Download the repo
* Install dependencies using `npm install`
* Run docker compose build
* Run docker compose up
* Go to http://localhost:3333/

## Developing and testing

* By default application is retrieving data from "https://burger-venue.herokuapp.com"
* To retrieve data from local repository change target url in proxy.conf.json to the following:
```
{
  "/proxy/burger-venue/api/*": {
    "target": "https://locahost:8080",
    "pathRewrite": {
      "^/proxy": ""
    },
    "secure": false,
    "loglevel": "debug",
    "changeOrigin": true
  }
}
```
* Backend application can be found here: https://gitlab.com/burger-venue/burger-venue-api 
* For additional development options check Nx chapter

## Nx

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**


## Quick Start & Documentation

[Nx Documentation](https://nx.dev/angular)

[10-minute video showing all Nx features](https://nx.dev/getting-started/intro)

[Interactive Tutorial](https://nx.dev/tutorial/01-create-application)

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are our core plugins:

- [Angular](https://angular.io)
  - `ng add @nrwl/angular`
- [React](https://reactjs.org)
  - `ng add @nrwl/react`
- Web (no framework frontends)
  - `ng add @nrwl/web`
- [Nest](https://nestjs.com)
  - `ng add @nrwl/nest`
- [Express](https://expressjs.com)
  - `ng add @nrwl/express`
- [Node](https://nodejs.org)
  - `ng add @nrwl/node`

There are also many [community plugins](https://nx.dev/community) you could add.

## Generate an application

Run `ng g @nrwl/angular:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `ng g @nrwl/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@venues/mylib`.

## Development server

Run `ng serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng g component my-component --project=my-app` to generate a new component.

## Build

Run `ng build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev/angular) to learn more.

